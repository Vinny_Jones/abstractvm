/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sCommandLine.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:49:42 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:49:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "abstractVM.hpp"

sCommandLine::sCommandLine(eCommand &cmd, int l, const IOperand *operand_ptr)
		: command(cmd), line(l), operand(operand_ptr)
{
}
