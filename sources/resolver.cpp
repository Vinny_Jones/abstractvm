/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolver.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:49:33 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:49:37 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "abstractVM.hpp"

const std::unique_ptr<Resolver> Resolver::m_instance = std::unique_ptr<Resolver>(new Resolver());

Resolver *		Resolver::get() {
	return (m_instance.get());
}

void	Resolver::run() {
	auto & commandsList = Lexer::get()->getCommandsList();
	m_container.clear();

	for (auto && command : commandsList) {
		performOperation(command);
	}
	if (!m_exitReached)
		throw std::logic_error("Input data must contain <exit> command.");
}

void	Resolver::performOperation(const sCommandLine & command) {
	if (m_exitReached) {
		throw std::logic_error("[Line " + std::to_string(command.line) + "] Command <" + Lexer::commands[command.command].first + "> is given after exit.");
	}
	switch (command.command) {
		case eCommand::Push:
			m_container.emplace_front(command.operand);
			return;
		case eCommand::Pop:
			pop(command.line);
			return;
		case eCommand::Dump:
			return (dump());
		case eCommand::Assert:
			return (assertStack(command));
		case eCommand::Add:
		case eCommand::Sub:
		case eCommand::Mul:
		case eCommand::Div:
		case eCommand::Mod:
			return(calculate(command));
		case eCommand::Print:
		{
			auto item = m_container.front();
			if (item->getType() != eOperandType::Int8) {
				throw std::logic_error("[Line " + std::to_string(command.line) + "] Print error. Value on the top of stack is not int8.");
			}
			std::cout << static_cast<signed char>(Operand<int8_t>::getNumber(item.get())) << std::endl;
			return;
		}
		case eCommand::Exit:
			m_exitReached = true;
			return;
		default:
			throw std::runtime_error("[Line " + std::to_string(command.line) + "] Unknown command " + std::to_string(command.command) + ".");
	}
}

void	Resolver::dump() const {
	std::cout << "Stack contents:";
	if (m_container.empty()) {
		std::cout << " empty";
	} else {
		for (auto && item : m_container) {
			std::cout << " " << item->toString();
		}
	}
	std::cout << std::endl;
}

const std::shared_ptr<const IOperand>	Resolver::pop(int line) {
	if (m_container.empty())
		throw std::logic_error("[Line " + std::to_string(line) + "] Pop on empty stack.");
	auto	returnValue = m_container.front();
	m_container.pop_front();
	return (returnValue);
}

void	Resolver::assertStack(const sCommandLine & command) const {
	if (m_container.empty()) {
		throw std::logic_error("[Line " + std::to_string(command.line) + "] Assert on empty stack.");
	}
	auto item = m_container.front();
	if (*item != *command.operand) {
		throw std::logic_error("[Line " + std::to_string(command.line) + "] Assert failed: " + item->toString() + " is not equal to " + command.operand->toString() + ".");
	}
}

void	Resolver::calculate(const sCommandLine & command) {
	if (m_container.size() < 2) {
		throw std::logic_error("[Line " + std::to_string(command.line) + "] Unable to run <" + Lexer::commands[command.command].first + "> on stack of size " + std::to_string(m_container.size()) + ".");
	}
	auto	rhs = pop(command.line);
	auto	lhs = pop(command.line);

	switch (command.command) {
		case eCommand::Add:
			return(m_container.emplace_front(*lhs + *rhs));
		case eCommand::Sub:
			return (m_container.emplace_front(*lhs - *rhs));
		case eCommand::Mul:
			return(m_container.emplace_front(*lhs * *rhs));
		case eCommand::Div:
			return(m_container.emplace_front(*lhs / *rhs));
		case eCommand::Mod:
			return(m_container.emplace_front(*lhs % *rhs));
		default:
			throw std::runtime_error("[Line " + std::to_string(command.line) + "] Unknown command " + std::to_string(command.command) + ".");
	}
}
