/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:49:14 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:49:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "abstractVM.hpp"

const std::string Lexer::strDelimeter = "\t\n\v\f\r ";

const std::array<commandType, Lexer::commandsCount> Lexer::commands = {{
	commandType{"push", eCommand::Push},		// Pushes the value at the top of the stack
	commandType{"pop", eCommand::Pop},			// Unstacks the value from the top of the stack.
	commandType{"dump", eCommand::Dump},    	// Displays each value of the stack
	commandType{"assert", eCommand::Assert},	// Asserts that the value at the top of the stack is equal to the one passed as parameter.
	commandType{"add", eCommand::Add},        	// Unstacks the first two values on the stack, adds them together and stacks the result.
	commandType{"sub", eCommand::Sub},        	// Unstacks the first two values on the stack, subtracts them, then stacks the result.
	commandType{"mul", eCommand::Mul},        	// Unstacks the first two values on the stack, multiplies them, then stacks the result.
	commandType{"div", eCommand::Div},        	// Unstacks the first two values on the stack, divides them, then stacks the result.
	commandType{"mod", eCommand::Mod},			// Unstacks the first two values on the stack, calculates the modulus, then stacks the result.
	commandType{"print", eCommand::Print},    	// Assert top of stack as 8-bit integer and displays its' ASCII representation.
	commandType{"exit",	eCommand::Exit},    	// Terminate the execution of the current program.
}};

const std::unique_ptr<Lexer> Lexer::m_instance = std::unique_ptr<Lexer>(new Lexer());

Lexer *     Lexer::get() {
	return (m_instance.get());
}

const std::list<sCommandLine> &	Lexer::getCommandsList() {
	return (m_commandsList);
}

void	    Lexer::run(std::istream * inputSource) {
	std::string				    inputBuffer;
	std::map<int, std::string>  errorMessages;

	m_commandsList.clear();
	m_currentLine = 1;
	m_fileMode = inputSource != &std::cin;
	while (!std::getline(*inputSource, inputBuffer).eof()) {
		if (inputSource->fail()) {
			throw std::ios_base::failure("Failed to read from input source.");
		}
		try {
			if (!parseLine(prepareParse(inputBuffer)))
				break;
		} catch (std::exception & exception) {
			errorMessages.insert(std::pair<int, std::string>(m_currentLine, exception.what()));
		}
		m_currentLine++;
	}

	for (auto && item : errorMessages) {
		std::cout << "Error at line " << item.first << ": " << item.second << std::endl;
	}
	if (!errorMessages.empty()) {
		throw std::runtime_error("Errors occurred while parsing input data.");
	}
}

bool        Lexer::parseLine(std::string & rawLine) {
	if (rawLine.empty())
		return (true);
	if (!m_fileMode && rawLine == ";;")
		return (false);

	size_t      wordDivider = rawLine.find_first_of(Lexer::strDelimeter);
	eCommand    command = resolveCommand(rawLine.substr(0, wordDivider));
	std::string param = wordDivider == std::string::npos ? "" : rawLine.substr(wordDivider);
	param.erase(0, param.find_first_not_of(Lexer::strDelimeter));

	checkParameter(command, param);
	if (param.empty()) {
		m_commandsList.emplace_back(command, m_currentLine, nullptr);
		return (true);
	}

	const IOperand * operand = parseOperand(param);
	m_commandsList.emplace_back(command, m_currentLine, operand);
	return (true);
}

std::string &   Lexer::prepareParse(std::string & str) {
	if (str.empty())
		return (str);
	size_t commentPosition = str.find_first_of(';');
	if (commentPosition < str.size() - 1 && str[commentPosition + 1] == ';' && !m_fileMode) {
		commentPosition = str.find_first_of(';', commentPosition + 2);
	}
	if (commentPosition != std::string::npos)
		str.erase(commentPosition);
	str.erase(0, str.find_first_not_of(Lexer::strDelimeter));
	str.erase(str.find_last_not_of(Lexer::strDelimeter) + 1);
	return (str);
}

eCommand        Lexer::resolveCommand(std::string && command) const {
	std::transform(command.begin(), command.end(), command.begin(), ::tolower);

	for (auto && item : commands) {
		if (command == item.first)
			return (item.second);
	}
	throw std::logic_error("Incorrect command <" + command + ">.");
}

void            Lexer::checkParameter(eCommand cmd, std::string & param) const {
	switch (cmd) {
	case eCommand::Pop:
	case eCommand::Dump:
	case eCommand::Add:
	case eCommand::Sub:
	case eCommand::Mul:
	case eCommand::Div:
	case eCommand::Mod:
	case eCommand::Print:
	case eCommand::Exit:
		if (!param.empty()) {
			throw std::logic_error("Unexpected parameter <" + param + "> is given for <" + commands[cmd].first + "> command.");
		}
		return;
	case eCommand::Push:
	case eCommand::Assert:
		if (param.empty()) {
			throw std::logic_error("Parameter for <" + commands[cmd].first + "> command is missing.");
		}
		return;
	default:
		;
	}
}

const IOperand *    Lexer::parseOperand(std::string & param) {
	size_t          bracket1 = param.find_first_of('(');
	eOperandType    type = parseOperandType(param.substr(0, bracket1));

	if (bracket1 == std::string::npos) {
		throw std::logic_error("Opening bracket for number is missing.");
	}
	param.erase(0, bracket1 + 1);
	size_t          bracket2 = param.find_first_of(')');
	if (bracket2 == std::string::npos) {
		throw std::logic_error("Closing bracket for number is missing.");
	}
	std::string     number = param.substr(0, bracket2);
	param.erase(0, bracket2 + 1);

	if (!param.empty()) {
		throw std::logic_error("Lexical error at <" + param + ">.");
	}

	checkOperandNumber(type, number);
	return (OperandFactory::get()->createOperand(type, number));
}

eOperandType    Lexer::parseOperandType(std::string && typeStr) const {
	std::transform(typeStr.begin(), typeStr.end(), typeStr.begin(), ::tolower);

	if (typeStr == "int8")
		return (eOperandType::Int8);
	if (typeStr == "int16")
		return (eOperandType::Int16);
	if (typeStr == "int32")
		return (eOperandType::Int32);
	if (typeStr == "float")
		return (eOperandType::Float);
	if (typeStr == "double")
		return (eOperandType::Double);

	throw std::logic_error("Incorrect operand type <" + typeStr + ">.");
}

void        Lexer::checkOperandNumber(eOperandType type, std::string & typeStr) const {
	if (typeStr.empty()) {
		throw std::logic_error("Number is empty.");
	}
	std::string::iterator   it = typeStr.begin();
	it = (*it == '+' || *it == '-') ? it + 1 : it;
	while (it != typeStr.end() && isdigit(*it))
		it++;
	if (it != typeStr.end() && (type == eOperandType::Float || type == eOperandType::Double)) {
		if (*it == ',' || *it == '.') {
			*it = '.';
			while (++it != typeStr.end() && isdigit(*it))
				;
		}
	}
	if (it == typeStr.end())
		return;
	std::string     errorStr = typeStr.substr(it - typeStr.begin());
	throw std::logic_error("Error while parsing number at \"" + errorStr + "\".");
}
