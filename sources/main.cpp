/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:49:20 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:49:22 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "abstractVM.hpp"

int		main(int argc, const char *argv[]) {
	try {
		if (argc > 2) {
			throw std::logic_error("Zero or one argument is required.");
		} else if (argc == 2) {
			std::ifstream	filestream(argv[1], std::ios::in);
			if (filestream.is_open()) {
				Lexer::get()->run(&filestream);
				filestream.close();
			}
			else
				throw std::logic_error("Unable to open " + std::string(argv[1]) + ".");
		} else {
			Lexer::get()->run(&std::cin);
		}
		Resolver::get()->run();

	} catch (std::exception & ex) {
		std::cout << "Program terminated due to exception thrown: " << ex.what() << std::endl;
	}

	return (0);
}
