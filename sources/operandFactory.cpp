/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operandFactory.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:49:28 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:49:29 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "abstractVM.hpp"

const std::unique_ptr<OperandFactory> OperandFactory::instance = std::unique_ptr<OperandFactory>(new OperandFactory());

const OperandFactory * OperandFactory::get() {
	return (instance.get());
}

OperandFactory::OperandFactory() {
#ifdef DEBUG_MODE
	std::cout << "[Debug mode] constructor OperandFactory called" << std::endl;
#endif

	operandFunctions[eOperandType::Int8] = &OperandFactory::createInt8;
	operandFunctions[eOperandType::Int16] = &OperandFactory::createInt16;
	operandFunctions[eOperandType::Int32] = &OperandFactory::createInt32;
	operandFunctions[eOperandType::Float] = &OperandFactory::createFloat;
	operandFunctions[eOperandType::Double] = &OperandFactory::createDouble;
}

const IOperand *	OperandFactory::createOperand(eOperandType type, const std::string & value) const {
	if (type < 0 || type >= operandFunctions.size()) {
		throw std::range_error("Operand type index error.");
	}
	return ((this->*operandFunctions[type])(value));
}

const IOperand *    OperandFactory::createInt8(const std::string & value) const {
	return (convertToType<int8_t, intmax_t>(value));
}

const IOperand *    OperandFactory::createInt16(const std::string &value) const {
	return (convertToType<int16_t, intmax_t>(value));
}

const IOperand *    OperandFactory::createInt32(const std::string &value) const {
	return (convertToType<int32_t, intmax_t>(value));
}

const IOperand *    OperandFactory::createFloat(const std::string &value) const {
	return (convertToType<float, long double>(value));
}

const IOperand *    OperandFactory::createDouble(const std::string &value) const {
	return (convertToType<double, long double>(value));
}

template <typename OperandType, typename ConvertType>
const IOperand *	OperandFactory::convertToType(const std::string & value) const {
	ConvertType         maxNumber;
	std::istringstream  convert(value);

	if (!(convert >> maxNumber) || convert.fail()) {
		throw std::runtime_error("Unable to parse " + value + " as valid number");
	}

	bool	isNegative = maxNumber < 0;
	if ((isNegative ? -maxNumber : maxNumber) > std::numeric_limits<OperandType>::max()) {
		throw std::range_error("Number overflows type limits.");
	}

	return (new Operand<OperandType>(static_cast<OperandType>(maxNumber)));
}
