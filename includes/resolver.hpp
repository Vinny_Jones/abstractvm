/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolver.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:50:22 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:50:24 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RESOLVER_HPP
# define RESOLVER_HPP

/* Resolver - singleton, used to perform math operations on data, prepared by Lexer */
class Resolver final {
public:
	Resolver(const Resolver & rhs) = delete;
	Resolver &	operator=(const Resolver & rhs) = delete;
#ifdef DEBUG_MODE
    virtual ~Resolver() {std::cout << "[Debug mode] destructor Resolver called" << std::endl;}
#else
    virtual ~Resolver() = default;
#endif


	static Resolver *	get();
	void				run();

private:
#ifdef DEBUG_MODE
	Resolver() {std::cout << "[Debug mode] constructor Resolver called" << std::endl;}
#else
	Resolver() = default;
#endif

	void 									performOperation(const sCommandLine & command);
	const std::shared_ptr<const IOperand>	pop(int line);
	void									dump() const;
	void									assertStack(const sCommandLine & command) const;
	void									calculate(const sCommandLine &command);

	static const std::unique_ptr<Resolver>		m_instance;
	std::deque<std::shared_ptr<const IOperand>> m_container;
	bool										m_exitReached;
};

#endif
