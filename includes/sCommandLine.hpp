/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sCommandLine.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:50:27 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:50:28 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOMMANDLINE_HPP
# define SCOMMANDLINE_HPP

enum eCommand {
	Push = 0,
	Pop,
	Dump,
	Assert,
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	Print,
	Exit
};

struct	sCommandLine {
	sCommandLine(eCommand & cmd, int l, const IOperand * operand_ptr);
	sCommandLine(const sCommandLine & rhs) = delete;
	sCommandLine & operator=(const sCommandLine & rhs) = delete;
    virtual ~sCommandLine() = default;

	const eCommand	                		command;
	const int 								line;
	const std::shared_ptr<const IOperand>	operand;
};

#endif
