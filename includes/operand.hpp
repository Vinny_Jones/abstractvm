/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operand.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:50:14 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:50:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERAND_HPP
# define OPERAND_HPP

# include "iOperand.hpp"
# include "operandFactory.hpp"

template <typename operandType>
class Operand final : public IOperand {
public:
	Operand<operandType>();
	explicit Operand<operandType>(operandType value);
	Operand(const Operand & rhs) = default;
	Operand &	operator=(const Operand & rhs) = default;

#ifdef DEBUG_MODE
	virtual ~Operand() {std::cout << "[Debug mode] destructor Operand called" << std::endl;};
#else
    virtual ~Operand() override = default;
#endif

	static operandType	getNumber(const IOperand *);

	int				    getPrecision() const final;
	eOperandType	    getType() const final;
	const std::string & toString() const final;

	const IOperand *	operator+(const IOperand & rhs) const final;
	const IOperand *	operator-(const IOperand & rhs) const final;
	const IOperand *	operator*(const IOperand & rhs) const final;
	const IOperand *	operator/(const IOperand & rhs) const final;
	const IOperand *	operator%(const IOperand & rhs) const final;
	bool                operator==(const IOperand & rhs) const final;
	bool                operator!=(const IOperand & rhs) const final;

private:
	enum eOperationType {
		Plus = 0,
		Substract,
		Multiply,
		Divide,
		Modulo
	};

	const IOperand *	performOperation(const IOperand * rhs, eOperationType operationType) const;
	template <typename rhsType>
	const IOperand *	calculateResult(const IOperand * rhs, eOperationType operationType) const;

	eOperandType	m_type;
	operandType		m_value;
	std::string     m_str;
};

template <typename operandType>
Operand<operandType>::Operand() : Operand(static_cast<operandType>(0)) {}

template <typename operandType>
Operand<operandType>::Operand(operandType value) : m_value(value) {
#ifdef DEBUG_MODE
	std::cout << "[Debug mode] constructor Operand called" << std::endl;
#endif

	auto & valueType = typeid(operandType);

	if (valueType == typeid(int8_t)) {
		m_type = eOperandType::Int8;
	} else if (valueType == typeid(int16_t)) {
		m_type = eOperandType ::Int16;
	} else if (valueType == typeid(int32_t)) {
		m_type = eOperandType::Int32;
	} else if (valueType == typeid(float)) {
		m_type = eOperandType::Float;
	} else if (valueType == typeid(double)) {
		m_type = eOperandType::Double;
	}
	else {
		throw std::runtime_error("Unknown type while initializing operand.");
	}

	std::ostringstream  stream;
	if (m_type == eOperandType::Float || m_type == eOperandType::Double)
		stream << std::defaultfloat << m_value;
	else
		stream << static_cast<int>(m_value);
	m_str = (m_value == 0) ? "0" : stream.str();
}

template <typename operandType>
int             Operand<operandType>::getPrecision() const {
	return (getType());
}

template <typename operandType>
eOperandType    Operand<operandType>::getType() const {
	return (m_type);
}

template <typename operandType>
operandType		Operand<operandType>::getNumber(const IOperand * input) {
	auto	target = dynamic_cast<const Operand<operandType> * >(input);

	if (target == nullptr) {
		throw std::runtime_error("Unable to cast IOperand into type Operand.");
	}
	return (target->m_value);
}

template <typename operandType>
const std::string & Operand<operandType>::toString() const {
	return (m_str);
}

template <typename operandType>
const IOperand *	Operand<operandType>::operator+(const IOperand & rhs) const {
	return (performOperation(&rhs, eOperationType::Plus));
}

template <typename operandType>
const IOperand *	Operand<operandType>::operator-(const IOperand & rhs) const {
	return (performOperation(&rhs, eOperationType::Substract));
}

template <typename operandType>
const IOperand *	Operand<operandType>::operator*(const IOperand &rhs) const {
	return (performOperation(&rhs, eOperationType::Multiply));
}

template <typename operandType>
const IOperand *	Operand<operandType>::operator/(const IOperand &rhs) const {
	return (performOperation(&rhs, eOperationType::Divide));
}

template <typename operandType>
const IOperand *	Operand<operandType>::operator%(const IOperand &rhs) const {
	return (performOperation(&rhs, eOperationType::Modulo));
}

template <typename operandType>
bool	Operand<operandType>::operator==(const IOperand & rhs) const {
	switch (rhs.getType()) {
		case eOperandType::Int8:
			return (m_value == Operand<int8_t>::getNumber(&rhs));
		case eOperandType::Int16:
			return (m_value == Operand<int16_t>::getNumber(&rhs));
		case eOperandType::Int32:
			return (m_value == Operand<int32_t>::getNumber(&rhs));
		case eOperandType::Double:
			return (m_value == Operand<double>::getNumber(&rhs));
		case eOperandType::Float:
			return (m_value == Operand<float>::getNumber(&rhs));
		default:
			throw std::logic_error("Unknown operand type used.");
	}
}

template <typename operandType>
bool	Operand<operandType>::operator!=(const IOperand & rhs) const {
	return (!(*this == rhs));
}

template <typename operandType>
const IOperand * Operand<operandType>::performOperation(const IOperand *rhs, eOperationType operationType) const {
	switch (rhs->getType()) {
	case eOperandType::Int8:
		return (calculateResult<int8_t>(rhs, operationType));
	case eOperandType::Int16:
		return (calculateResult<int16_t>(rhs, operationType));
	case eOperandType::Int32:
		return (calculateResult<int32_t>(rhs, operationType));
	case eOperandType::Float:
		return (calculateResult<float>(rhs, operationType));
	case eOperandType::Double:
		return (calculateResult<double>(rhs, operationType));
	default:
		throw std::logic_error("Unknown operationType used to perform calculate.");
	}
}

template <typename operandType>
template <typename rhsType>
const IOperand *	Operand<operandType>::calculateResult(const IOperand *rhs, Operand::eOperationType operationType) const {
	eOperandType    resultType = (m_type > rhs->getType() ? m_type : rhs->getType());
	rhsType			rhsNumber = Operand<rhsType>::getNumber(rhs);

	switch (operationType) {
	case eOperationType::Plus:
		return (OperandFactory::get()->createOperand(resultType, std::to_string(m_value + rhsNumber)));
	case eOperationType::Substract:
		return (OperandFactory::get()->createOperand(resultType, std::to_string(m_value - rhsNumber)));
	case eOperationType::Multiply:
		return (OperandFactory::get()->createOperand(resultType, std::to_string(m_value * rhsNumber)));
	case eOperationType::Divide:
		if (rhsNumber == 0) {
			throw std::domain_error("Division by zero.");
		}
		return (OperandFactory::get()->createOperand(resultType, std::to_string(m_value / rhsNumber)));
	case eOperationType::Modulo:
		if (rhsNumber == 0) {
			throw std::domain_error("Division by zero.");
		}
		return (OperandFactory::get()->createOperand(resultType, std::to_string(fmod(m_value, rhsNumber))));
	default:
		throw std::logic_error("Unknown operationType used to perform calculate.");
	}
}

#endif
