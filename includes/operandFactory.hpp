/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operandFactory.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:50:18 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:50:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERANDFACTORY_HPP
# define OPERANDFACTORY_HPP

/* Operand factory - singleton, used for creating operands */
class OperandFactory final {
	typedef const IOperand * (OperandFactory::* functionPointersArrayType)(const std::string &) const;
public:
	OperandFactory(OperandFactory const & rhs) = delete;
	OperandFactory &	operator=(OperandFactory const & rhs) = delete;

#ifdef DEBUG_MODE
	virtual ~OperandFactory() {std::cout << "[Debug mode] destructor OperandFactory called" << std::endl;}
#else
    virtual ~OperandFactory() = default;
#endif

	static const OperandFactory *	get();

	/* The amount of operand function pointers is hard-coded
	 * and must be the same as number of eOperandType enum elements */
	static const int functionCount = 5;

	const IOperand *	createOperand(eOperandType type, const std::string & value) const;

private:
	OperandFactory();
	static const std::unique_ptr<OperandFactory>	instance;

	std::array<functionPointersArrayType, OperandFactory::functionCount> operandFunctions;

	const IOperand *	createInt8(const std::string & value) const;
	const IOperand *	createInt16(const std::string & value) const;
	const IOperand *	createInt32(const std::string & value) const;
	const IOperand *	createFloat(const std::string & value) const;
	const IOperand *	createDouble(const std::string & value) const;

	template <typename OperandType, typename ConvertType>
	const IOperand *	convertToType(const std::string &value) const;
};


#endif
