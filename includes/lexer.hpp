/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:50:09 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:50:10 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEXER_HPP
# define LEXER_HPP

typedef const std::pair<std::string, eCommand> commandType;

/* Lexer - singleton, used to get and check input */
class Lexer final{
public:
	static const std::string strDelimeter;
	static const int commandsCount = 11;
	static const std::array<commandType, Lexer::commandsCount> commands;
	static Lexer *	get();

	Lexer(const Lexer & rhs) = delete;
	Lexer &	operator=(const Lexer & rhs) = delete;
#ifdef DEBUG_MODE
    virtual ~Lexer() {std::cout << "[Debug mode] destructor Lexer called" << std::endl;}
#else
    virtual ~Lexer() = default;
#endif

	void							run(std::istream * inputSource);
	const std::list<sCommandLine> &	getCommandsList();

private:
#ifdef DEBUG_MODE
	Lexer() {std::cout << "[Debug mode] constructor Lexer called" << std::endl;}
#else
	Lexer() = default;
#endif

	bool 	            parseLine(std::string & rawLine);
	std::string &       prepareParse(std::string & rawLine);
	eCommand            resolveCommand(std::string && command) const;
	void                checkParameter(eCommand cmd, std::string &param) const;
	const IOperand *    parseOperand(std::string & param);
	eOperandType        parseOperandType(std::string && typeStr) const;
	void                checkOperandNumber(eOperandType type, std::string & typeStr) const;

	static const std::unique_ptr<Lexer> m_instance;
	std::list<sCommandLine>        		m_commandsList;
	bool                                m_fileMode;
	int                                 m_currentLine;
};


#endif
