/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iOperand.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:50:03 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:50:05 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IOPERAND_H
# define IOPERAND_H

enum eOperandType {
	Int8 = 0,
	Int16,
	Int32,
	Float,
	Double
};

class IOperand {
public:
	virtual int				getPrecision() const = 0;									// Precision of the type of the m_instance
	virtual eOperandType	getType() const = 0;										// Type of the m_instance

	virtual const IOperand *	operator+(const IOperand & rhs) const = 0;				// Sum
	virtual const IOperand *	operator-(const IOperand & rhs) const = 0;				// Difference
	virtual const IOperand *	operator*(const IOperand & rhs) const = 0;				// Product
	virtual const IOperand *	operator/(const IOperand & rhs) const = 0;				// Quotient
	virtual const IOperand *	operator%(const IOperand & rhs) const = 0;				// Modulo
	virtual bool                operator==(const IOperand & rhs) const = 0;				// Equal
	virtual bool                operator!=(const IOperand & rhs) const = 0;				// Not equal

	virtual const std::string & toString() const = 0;									// String representation of the m_instance

	virtual ~IOperand() = default;

};

#endif
