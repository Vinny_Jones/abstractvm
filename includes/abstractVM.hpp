/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   abstractVM.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/14 23:49:57 by apyvovar          #+#    #+#             */
/*   Updated: 2018/05/14 23:49:59 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACTVM_HPP
# define ABSTRACTVM_HPP

# include <array>
# include <iostream>
# include <fstream>
# include <string>
# include <list>
# include <map>
# include <deque>
# include <memory>
# include <sstream>
# include <limits>
# include <iomanip>
# include <typeinfo>
# include <cmath>
# include <cfloat>
# include <algorithm>
# include "iOperand.hpp"

template <typename operandType>
class Operand;

# include "operandFactory.hpp"
# include "operand.hpp"
# include "sCommandLine.hpp"
# include "lexer.hpp"
# include "resolver.hpp"

#endif
