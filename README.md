##AbstractVM is a machine that uses a stack to compute simple arithmetic expressions. 
###These arithmetic expressions are provided to the machine as basic assembly programs.

~~~~
$./AbstractVM 

push int8(23) 
push int32(334343) 
push float(223.3422) 
push double(23.3434) 
push int16(434) 
push int8(33) 
add 
sub 
mul 
div 
mod 
dump 
exit 
;; 
$Stack contents: 2.75463
~~~~