# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/05/14 23:48:57 by apyvovar          #+#    #+#              #
#    Updated: 2018/05/14 23:49:01 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#########################     Basic     ########################################
FLAGS = -Wall -Wextra -Werror -std=c++11
CC = clang++ $(FLAGS)
#########################     Filler    ########################################
NAME = AbstractVM
INC = includes
SRC_DIR = sources/
SRC_FILES = main.cpp operandFactory.cpp lexer.cpp resolver.cpp sCommandLine.cpp

SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $@

.cpp.o: $(SRC)
	$(CC) -I $(INC) -c $^ -o $@

clean:
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(NAME)

re: fclean all